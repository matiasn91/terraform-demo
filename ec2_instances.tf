resource "aws_instance" "server_http" {
  ami                         = "${var.iam_id}"
  instance_type               = "t2.micro"
  count                       = 1
  subnet_id                   = "${aws_subnet.subnet1.id}"
  associate_public_ip_address = true

  vpc_security_group_ids = ["${aws_security_group.sg-1.id}", "${aws_security_group.sg-2.id}"]
  private_ip             = "10.0.10.10"
  key_name               = "${aws_key_pair.key-class1.id}"
  user_data = "${file("userdata.sh")}"


  tags = {
    Name  = "Server1"
    Owner = "terraform"
    Env   = "dev"
  }
}
