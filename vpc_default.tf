provider "aws" {
  region = var.region_name
}

resource "aws_vpc" "main1" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true


  tags = {
    Name = "Main VPC 1"
  }
}


resource "aws_subnet" "subnet1" {
  vpc_id                  = aws_vpc.main1.id
  cidr_block              = var.subnet1_cidr_block
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "subnet us-east-1a"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id                  = aws_vpc.main1.id
  cidr_block              = var.subnet2_cidr_block
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"

  tags = {
    Name = "subnet us-east-1b"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main1.id

  tags = {
    Name = "Creo nuestro Internet Gateway"
  }
}

resource "aws_route_table" "r" {
  vpc_id = aws_vpc.main1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table_association" "table_subnet1" {
    subnet_id = aws_subnet.subnet1.id
    route_table_id = aws_route_table.r.id
}

resource "aws_route_table_association" "table_subnet2" {
    subnet_id = aws_subnet.subnet2.id
    route_table_id = aws_route_table.r.id
}