variable "region_name" {
  description = "La region por defecto"
  default     = "us-east-1"
}

variable "vpc_cidr_block" {
  description = "bloque de ip para el vpc"
  default     = "10.0.0.0/16"
}

variable "subnet1_cidr_block" {
  description = "bloque de ip para la subnet1"
  default     = "10.0.10.0/24"
}

variable "subnet2_cidr_block" {
  description = "bloque de ip para la subnet2"
  default     = "10.0.20.0/24"
}

variable "iam_id" {
  description = "Indicamos el IAM que vamos a utilizar"
  default     = "ami-04b9e92b5572fa0d1"
}

